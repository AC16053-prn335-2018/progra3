/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uesocc.edu.sv.ingenieria.prn335.boundary;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author cristian
 */
public class UtilidadesTest {
    
    public UtilidadesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getTitle method, of class Utilidades.
     */
    @Test
    public void testGetTitle() {
        System.out.println("getTitle");
        String texto = "Hola jovenes de progra 2.5 que tal estan";
        Utilidades instance = new Utilidades();
        String expResult = "HOLA JOVENES DE PROG";
        String result = instance.getTitle(texto);
        assertEquals(expResult, result);
        
    }
    
}
