
package uesocc.edu.sv.ingenieria.prn335.boundary;

/**
 * 
 * @author cristian
 */
public class Utilidades {
    
    /**
     * Este metodo agarra solo 20 letras y las hace mayusculas
     * @param texto es una cadena de texto 
     * @return la misma cadena de texto pero solo las 20 primeras letras en mayusculas
     */
    public String getTitle(String texto){
        texto = texto.substring(0, 20);
        texto = texto.toUpperCase();
    return texto;
    }
}
